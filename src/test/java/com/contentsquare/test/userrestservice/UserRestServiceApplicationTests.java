package com.contentsquare.test.userrestservice;

import com.contentsquare.test.userrestservice.countryip.CountryIp;
import com.contentsquare.test.userrestservice.countryip.CountryIpRepository;
import com.contentsquare.test.userrestservice.user.User;
import com.contentsquare.test.userrestservice.user.UserService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.net.InetAddress;
import java.net.UnknownHostException;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

@RunWith(SpringRunner.class)
@SpringBootTest
@WebAppConfiguration
public class UserRestServiceApplicationTests {

    @Autowired
    private WebApplicationContext ctx;

    @Autowired
    private UserService userService;

    @Autowired
    private CountryIpRepository countryIpRepository;

    private MockMvc mockMvc;

    @Before
    public void init() throws UnknownHostException {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.ctx).build();
        CountryIp mdIp = new CountryIp();
        mdIp.setIpAddress("127.0.0.1/16");
        mdIp.setCountry("Moldova");
        mdIp = countryIpRepository.save(mdIp);

        CountryIp simpleIp = new CountryIp();
        simpleIp.setIpAddress("192.168.1.1");
        simpleIp.setCountry("Simple");
        simpleIp = countryIpRepository.save(simpleIp);

        User md = new User();
        md.setFirstName("firstName");
        md.setId("md");
        md.setLastName("lastName");
        md.setIp(InetAddress.getByName("127.0.0.2"));
        md = userService.saveUser(md);

        User simple = new User();
        simple.setFirstName("firstName");
        simple.setId("simple");
        simple.setLastName("lastName");
        simple.setIp(InetAddress.getByName("192.168.1.1"));
        simple = userService.saveUser(simple);
    }

    @Test
    public void shuldCheckSubnetIp() throws Exception {
        mockMvc.perform(get("/users/md"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.country", is("Moldova")));
    }

    @Test
    public void shuldCheckSimpleIp() throws Exception {
        mockMvc.perform(get("/users/simple"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.country", is("Simple")));
    }

}
