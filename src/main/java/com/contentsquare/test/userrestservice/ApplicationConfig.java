package com.contentsquare.test.userrestservice;

import com.contentsquare.test.userrestservice.countryip.CountryIp;
import com.contentsquare.test.userrestservice.countryip.CountryIpRepository;
import com.google.common.net.InetAddresses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.supercsv.io.CsvBeanReader;
import org.supercsv.prefs.CsvPreference;

import javax.annotation.PostConstruct;
import java.io.FileReader;
import java.io.IOException;
import java.net.InetAddress;

/**
 * Configuration class
 */
@Slf4j
@Configuration
public class ApplicationConfig {

    @Autowired
    CountryIpRepository countryIpRepository;
    @Value("${geoip.csv.path}")
    private String csvPath;

    @Profile("!tests")
    @PostConstruct
    void initCountryIps() throws IOException {
        CsvBeanReader beanReader = new CsvBeanReader(new FileReader(csvPath), CsvPreference.STANDARD_PREFERENCE);
        log.info("Started country ips database provisioning ...");
        CountryIp countryIp = null;
        while ((countryIp = beanReader.read(CountryIp.class, "ipAddress", "country")) != null) {
            String ipAddress = countryIp.getIpAddress();
            if(ipAddress.contains("*")){
                int starIndex = ipAddress.indexOf("*");
                //if joker, insert all ips in database
                for(int i = 0; i < 256; i++){
                    CountryIp newCountryIp = new CountryIp();
                    newCountryIp.setCountry(countryIp.getCountry());
                    newCountryIp.setIpAddress(ipAddress.substring(0, starIndex) + i + ipAddress.substring(starIndex + 1, ipAddress.length()));
                   countryIpRepository.save(newCountryIp);
                }
            }
            else {
                countryIpRepository.save(countryIp);
            }
        }
        log.info("Finished Country Ip database provisioning!");
    }
}
