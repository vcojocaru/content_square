package com.contentsquare.test.userrestservice.countryip;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

/**
 * Country ip repository
 */
public interface CountryIpRepository extends JpaRepository<CountryIp, Long>, QueryDslPredicateExecutor<CountryIp> {
}
