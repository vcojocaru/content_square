package com.contentsquare.test.userrestservice.countryip;

import com.google.common.net.InetAddresses;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.net.util.SubnetUtils;
import org.springframework.data.jpa.domain.AbstractPersistable;

import javax.persistence.Entity;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * Country Ip Representation
 */
@Slf4j
@Entity
@Data
@Table
@AllArgsConstructor
@NoArgsConstructor
public class CountryIp extends AbstractPersistable<Long> {

    private String country;
    private String ipAddress;
    private int startIp;
    private int endIp;

    // This function is used to fill start and end ip ranges before persisting data
    @PrePersist
    void setRanges() {
        try {
            if (ipAddress.contains("/")) {
                SubnetUtils utils = new SubnetUtils(ipAddress);
                String[] ipAndMask = ipAddress.split("/");
                //set start and end ip ranges

                this.startIp = InetAddresses.coerceToInteger(InetAddress.getByName(utils.getInfo().getLowAddress()));
                this.endIp = InetAddresses.coerceToInteger(InetAddress.getByName(utils.getInfo().getHighAddress()));
                //set the right Ip
                this.setIpAddress(ipAndMask[0]);
            } else {
                this.startIp = InetAddresses.coerceToInteger(InetAddress.getByName(ipAddress));
                this.endIp = this.startIp;
            }

        } catch (UnknownHostException e) {
            log.info("Cannot load wrong formatted ip: {}, country: {}", ipAddress, country);
        }
    }
}
