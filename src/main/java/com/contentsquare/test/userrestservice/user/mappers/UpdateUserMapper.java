package com.contentsquare.test.userrestservice.user.mappers;

import com.contentsquare.test.userrestservice.user.User;
import com.contentsquare.test.userrestservice.user.json.UpdateRequest;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.function.BiFunction;

import static com.contentsquare.test.userrestservice.user.UserErrorCodes.UNKNOWN_HOST_NAME;

/**
 * Update bi-function that adds new updated information to existing user
 */
public class UpdateUserMapper implements BiFunction<UpdateRequest, User, User> {
    @Override
    public User apply(UpdateRequest updateRequest, User userToUpdate) {
        //check mandatory fields and update user only if they aren't null
        if (updateRequest.getLastName() != null) {
            userToUpdate.setLastName(updateRequest.getLastName());
        }
        if (updateRequest.getIp() != null) {
            InetAddress inetAddress = null;
            try {
                inetAddress = InetAddress.getByName(updateRequest.getIp());
                userToUpdate.setIp(inetAddress);
            } catch (UnknownHostException e) {
                throw new RuntimeException(UNKNOWN_HOST_NAME.getMessage(), e);
            }
        }

        //update optional fields even if null (if not present in query, to be deleted)
        userToUpdate.setAge(updateRequest.getAge());
        userToUpdate.setFirstName(updateRequest.getFirstName());
        return userToUpdate;
    }
}
