package com.contentsquare.test.userrestservice.user;

import com.contentsquare.test.userrestservice.user.json.CreateRequest;
import com.contentsquare.test.userrestservice.user.json.UpdateRequest;
import com.contentsquare.test.userrestservice.user.json.UserResponse;
import com.contentsquare.test.userrestservice.user.mappers.CreateUserMapper;
import com.contentsquare.test.userrestservice.user.mappers.UpdateUserMapper;
import com.contentsquare.test.userrestservice.user.mappers.UserResponseMapper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpServerErrorException;

import javax.validation.Valid;

import static com.contentsquare.test.userrestservice.user.UserErrorCodes.USER_NOT_FOUND;

/**
 * Simple controller for user REST API
 */
@Slf4j
@RestController
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class UserController implements UserApi {

    private UserService userService;

    /**
     * @param userId user identifier
     * @return corresponding user
     */
    @RequestMapping(value = "/users/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public UserResponse getUser(@PathVariable("id") String userId) {
        log.info("Getting user with id: {}", userId);
        return new UserResponseMapper().apply(userService.getUser(userId));
    }

    @Override
    @RequestMapping(value = "/users", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public UserResponse createUser(@RequestBody @Valid CreateRequest createRequest) {
        log.info("Creating user with request: {}", createRequest);
        return new UserResponseMapper().apply(userService.saveUser(new CreateUserMapper().apply(createRequest)));
    }

    @Override
    @RequestMapping(value = "/users/{id}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public UserResponse updateUser(@RequestBody @Valid UpdateRequest updateRequest, @PathVariable("id") String userId) {
        log.info("Creating user with request: {}", updateRequest);
        User userToUpdate = userService.getUser(userId);
        if (userToUpdate == null) {
            throw new HttpServerErrorException(USER_NOT_FOUND.getStatus(), USER_NOT_FOUND.getMessage());
        }
        return new UserResponseMapper().apply(userService.updateUser(new UpdateUserMapper().apply(updateRequest, userToUpdate)));
    }
}
