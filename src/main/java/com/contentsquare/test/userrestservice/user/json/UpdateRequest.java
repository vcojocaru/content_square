package com.contentsquare.test.userrestservice.user.json;

import lombok.Data;
import org.apache.http.conn.util.InetAddressUtils;

import javax.validation.constraints.AssertTrue;

/**
 * Update user request
 */
@Data
public class UpdateRequest {

    private String lastName;

    private String ip;

    private String firstName;

    private Integer age;

    @AssertTrue
    private boolean isIpValid() {
        if (this.ip != null) {
            return InetAddressUtils.isIPv4Address(this.ip);
        } else {
            return true;
        }

    }
}
