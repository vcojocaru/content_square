package com.contentsquare.test.userrestservice.user;

import com.contentsquare.test.userrestservice.countryip.CountryIp;
import com.contentsquare.test.userrestservice.countryip.CountryIpRepository;
import com.contentsquare.test.userrestservice.countryip.QCountryIp;
import com.querydsl.core.BooleanBuilder;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import static com.contentsquare.test.userrestservice.countryip.QCountryIp.countryIp;

/**
 * User service used to defines all methods used to manage the user data
 */
@Service
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class UserService {

    private UserRepository userRepository;
    private CountryIpRepository countryIpRepository;

    Page<User> findUsers(Pageable p) {
        return userRepository.findAll(p);
    }

    public User getUser(String userId) {
        BooleanBuilder query = new BooleanBuilder();
        query.and(QUser.user.id.eq(userId));
        return userRepository.findOne(query);
    }

    public User saveUser(User user) {
        QCountryIp qCountryIp = countryIp;
        BooleanBuilder where = new BooleanBuilder();
        where.and(qCountryIp.startIp.loe(user.getIpInteger()).and(qCountryIp.endIp.goe(user.getIpInteger())));
        CountryIp countryIp = countryIpRepository.findOne(where);
        if (countryIp != null) {
            user.setCountry(countryIp.getCountry());
        }
        return userRepository.save(user);
    }

    public User updateUser(User user) {
        return userRepository.save(user);
    }
}
