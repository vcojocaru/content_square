package com.contentsquare.test.userrestservice.user.json;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.io.Serializable;

/**
 * User Response representation
 */
@Data
@JsonInclude(JsonInclude.Include.NON_ABSENT)
public class UserResponse implements Serializable {

    private String id;

    private String lastName;

    private String ip;

    private String firstName;

    private String country;

    private Integer age;
}
