package com.contentsquare.test.userrestservice.user;

import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;

/**
 * User error codes.
 */
@AllArgsConstructor
public enum UserErrorCodes {

    USER_NOT_FOUND(HttpStatus.NOT_FOUND, "User not found in database"),
    UNKNOWN_HOST_NAME(HttpStatus.BAD_REQUEST, "Unknown ip host");

    private HttpStatus status;
    private String message;

    public String getMessage() {
        return this.message;
    }

    public HttpStatus getStatus() {
        return this.status;
    }
}
