package com.contentsquare.test.userrestservice.user.mappers;

import com.contentsquare.test.userrestservice.user.User;
import com.contentsquare.test.userrestservice.user.json.CreateRequest;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.function.Function;

import static com.contentsquare.test.userrestservice.user.UserErrorCodes.UNKNOWN_HOST_NAME;

/**
 * Simple mapper between {@link CreateRequest} and {@link User}
 */
public class CreateUserMapper implements Function<CreateRequest, User> {
    @Override
    public User apply(CreateRequest createRequest) {
        User userToCreate = new User();
        //mandatory params
        userToCreate.setId(createRequest.getId());
        InetAddress ip = null;
        try {
            ip = Inet4Address.getByName(createRequest.getIp());
            userToCreate.setIp(ip);
        } catch (UnknownHostException e) {
            throw new RuntimeException(UNKNOWN_HOST_NAME.getMessage(), e);
        }
        userToCreate.setLastName(createRequest.getLastName());

        //optional ones
        userToCreate.setAge(createRequest.getAge());
        userToCreate.setFirstName(createRequest.getFirstName());

        return userToCreate;
    }
}
