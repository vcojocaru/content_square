package com.contentsquare.test.userrestservice.user;

import com.google.common.net.InetAddresses;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.net.InetAddress;

/**
 * User Entity
 */
@Data
@NoArgsConstructor
@Entity
@Table
public class User {

    @NotNull
    @Id
    @Column(nullable = false, unique = true)
    private String id;

    @NotEmpty
    @Size(max = 40)
    private String lastName;

    @NotNull
    private InetAddress ip;

    @Size(max = 40)
    private String firstName;

    private String country;

    private Integer age;

    public int getIpInteger() {
        return InetAddresses.coerceToInteger(ip);
    }
}
