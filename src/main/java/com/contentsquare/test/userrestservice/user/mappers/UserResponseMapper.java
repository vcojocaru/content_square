package com.contentsquare.test.userrestservice.user.mappers;

import com.contentsquare.test.userrestservice.user.User;
import com.contentsquare.test.userrestservice.user.json.UserResponse;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.function.Function;

/**
 * User Response mapper
 */
@JsonInclude(JsonInclude.Include.NON_ABSENT)
public class UserResponseMapper implements Function<User, UserResponse> {
    @Override
    public UserResponse apply(User user) {
        UserResponse response = new UserResponse();
        response.setId(user.getId());
        response.setLastName(user.getLastName());
        response.setFirstName(user.getFirstName());
        response.setAge(user.getAge());
        response.setIp(user.getIp().getHostAddress());
        response.setCountry(user.getCountry());

        return response;
    }
}
