package com.contentsquare.test.userrestservice.user;

import com.contentsquare.test.userrestservice.user.json.CreateRequest;
import com.contentsquare.test.userrestservice.user.json.UpdateRequest;
import com.contentsquare.test.userrestservice.user.json.UserResponse;

/**
 * Interface that describes the user API
 */
public interface UserApi {

    /**
     * @param userId user identifier
     * @return corresponding user
     */
    UserResponse getUser(String userId);

    /**
     * @param createRequest create user request body
     * @return created user
     */
    UserResponse createUser(CreateRequest createRequest);

    /**
     * @param updateRequest update user request body
     * @param id            user's unique identifier
     * @return updated user
     */
    UserResponse updateUser(UpdateRequest updateRequest, String id);
}
