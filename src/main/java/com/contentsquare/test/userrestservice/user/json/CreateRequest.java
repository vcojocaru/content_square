package com.contentsquare.test.userrestservice.user.json;

import lombok.Data;
import org.apache.http.conn.util.InetAddressUtils;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.Size;

/**
 * Create user request json
 */
@Data
public class CreateRequest {

    @NotEmpty
    private String id;

    @NotEmpty
    @Size(max = 40)
    private String lastName;

    @NotEmpty
    private String ip;

    @Size(max = 40)
    private String firstName;

    private Integer age;

    @AssertTrue
    private boolean isIpValid() {
        return InetAddressUtils.isIPv4Address(this.ip);
    }
}
